
This module requires version 1.3.0 or higher of xcache http://xcache.lighttpd.net/. 
Do not install if your xcache version is lower than 1.3.0

add the following php code to site settings.php

$conf['cache_inc'] = './sites/mysite/modules/xcache_cachepage/xcache_cachepage.inc';
$conf['page_cache_fastpath'] = TRUE;

enable the module